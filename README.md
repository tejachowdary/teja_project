To develop the application I have used the SUMO (Simulation of Urban Mobility) software for implementing this VANET application.
I have installed the SUMO software, MOVE.jar file.
To implement this simulation, also requires java development tool kit.
This simulation can also be done in Ubuntu and windows. 
But if you want to work with NS2 we need to do this simulation in Ubuntu itself. 
I did not worked with NS2, but I implemented this in Ubuntu.
I have installed the Ubuntu software in virtual box. 
To implement this vehicular ad-hoc network road simulation, I need a road map. 
For that I have a designed a road map with 5 junctions.
I named the junctions as nodes. I connected the nodes with edges. These edges are nothing but roads.  
In the command prompt I have executed the command to run the MOVE.jar file.
When I have done that I got the page called rapid generation of realistic simulation for VANET. 
In that I choose the mobility model. From there I got a page with multiple options like node edge and so on.
When I click on edge, I can create the edges between the junctions. When I click on node I can create the junctions.
 Next to generate a road map I have set the configuration with node file and edge file and to save that output, we need to create an output file with .net.xml extension. 
 All the node and edge files will also be saved in the form of xml files.
 The vehicles can choose any root to reach to the destination. 
 But in my simulation I have kept some of the source and destination routes for the vehicles. 
 For that I have created a flow and turn table and save them with .xml extension. 
 I also kept some priority for the number of vehicles to choose that lane to reach the destination. 
 In my simulation I also kept the speed limit as 75 and the road in this network are two lane roads.
 After doing all the requirements for the simulation I got created the vehicles in a network with the help of the two flow and turn files.
 After setting all the configuration settings when we click on the visualization button, the control will be automatically moved to SUMO graphical user interface. 
 With the help of that interface the simulation can be executed and we can observe the movement of vehicles. 